import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter !== undefined) {
    fighterElement.appendChild(createFighterImage(fighter));
  
    const description = createElement({
      tagName: 'div',
      className: 'fighter-preview___props'
    });
  
    fighterElement.appendChild(description);
    for (const [name, value] of Object.entries(fighter)) {
      if (name != 'source' && name != '_id')
        description.appendChild(createFighterInfo(name, value));
    }
  }
  return fighterElement;
}

const createFighterInfo = (name, value) => {
  const descriptionElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___props-block'
  });
  descriptionElement.innerHTML = `${name}: ${value};`;
  return descriptionElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
